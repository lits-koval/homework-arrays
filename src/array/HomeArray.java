package array;

import java.util.Random;

public class HomeArray {

    static int arraySize = 10; // размер массива

    public static void main(String[] args) {

        HomeArray array = new HomeArray();
        int[] startArray = array.initArray(arraySize);
        int[] reverseArray = array.reverseArray(startArray);
        int[] clearArray = array.clearArray(reverseArray);

        System.out.print("Начальный массив: ");
        for (int i = 0; i < arraySize; i++) {
            System.out.print(startArray[i]+" ");
        }
        System.out.println();

        System.out.print("Перевернутый массив: ");
        for (int i = 0; i < arraySize; i++) {
            System.out.print(reverseArray[i]+" ");
        }
        System.out.println();

        System.out.print("Очищенный от дубликатов массив: ");
        for (int i = 0; i < clearArray.length; i++) {
            System.out.print(clearArray[i]+" ");
        }
        System.out.println();
    }

//    инициализация массива
    int[] initArray(int arraySize){
        int[] array = new int[arraySize];
        for (int i = 0; i < arraySize; i++) {
            array[i] = new Random().nextInt(arraySize);
        }
        return array;
    }

//    реверс массива
    int[] reverseArray(int[] array){
        int[] reverseArray = new int[arraySize];
        for (int i = 0; i < arraySize; i++) {
            reverseArray[i] = array[arraySize-i-1];
        }
        return reverseArray;
    }

//  удаление дубликатов:
    int[] clearArray(int[] array) {
        boolean copy;
        int c = 0; // компилятор требует инициализацию значением
        int[] tmpArray = new int[arraySize];

        // временный массив без дубликатов:
        for (int i = 0; i < arraySize; i++) {
            copy = false;
            for (int j = 0; j < i-c; j++) {
                if (array[i]==tmpArray[j]){
                    copy = true;
                    c++;
                }
            }
            if (!copy) {
                tmpArray[i-c] = array[i];
            }
        }

        //очищенный массив:
        int[] clearArray = new int[arraySize-c];
        for (int i = 0; i < arraySize-c ; i++) {
            clearArray[i] = tmpArray[i];
        }
        return clearArray;
    }

}
